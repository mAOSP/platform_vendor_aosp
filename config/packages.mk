# Required packages
PRODUCT_PACKAGES += \
    Basic \
    CellBroadcastReceiver \
    Development \
    LatinIME

# Extra tools
PRODUCT_PACKAGES += \
    openvpn \
    e2fsck \
    mke2fs \
    tune2fs
